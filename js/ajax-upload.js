jQuery(document).ready(function () {
	var img_zone = document.getElementById('img-zone'),		
	collect = {
		filereader: typeof FileReader != 'undefined',
		zone: 'draggable' in document.createElement('span'),
		formdata: !!window.FormData
	}, 
	acceptedTypes = {
		'image/png': true,
		'image/jpeg': true,
		'image/jpg': true,
		'image/gif': true
	};
	
	// Function to show messages
	function ajax_msg(status, msg) {
		var the_msg = '<div class="alert alert-'+ (status ? 'success' : 'danger') +'">';
		the_msg += '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
		the_msg += msg;
		the_msg += '</div>';
		$(the_msg).insertBefore(img_zone);
	}
	
	// Function to upload image through AJAX
	function ajax_upload(input,files,events) {	
        
		var img = "";
		var img_preview = $('#img-preview');
		var formData = new FormData();
		if(files.length>0){
			for (var i = 0; i < files.length; i++) {
				formData.append('img_file[]', files[i]);
				var imgName = files[i].name;
				var reader = new FileReader();
				reader.onload = function (e) {
					img += '<div class="col-sm-3 photos"><div class="thumbnail"><img style="height: 100px;width: 100px" src="'+e.target.result+'" title="'+imgName+'"></div></div>';
					$('#img_preview').html(img);
				}
				reader.readAsDataURL(files[i]);
			}
			console.log("formData", formData);
		}else{
			ajax_msg(false, 'An error has occured while uploading files.'); 	
		}			
	}
	
	// Call AJAX upload function on drag and drop event
	function dragHandle(element) {
		element.ondragover = function () { return false; };
		element.ondragend = function () { return false; };
		element.ondrop = function (e) { 
			e.preventDefault(); console.log("drag");
			ajax_upload(e,e.dataTransfer.files,"drag");
		}  		
	}
	
	if (collect.zone) {  		
		dragHandle(img_zone);
	} 
	else {
		alert("Drag & Drop isn't supported, use Open File Browser to upload photos.");			
	}

	// Call AJAX upload function on image selection using file browser button
	$(document).on('change', '.box .media .drag-file:file', function(e) { 
		console.log("change");
		ajax_upload(this,this.files,"click");			
	});

	$(document).on('click', '.box .media .drag-file', function(e) {
		console.log("click");
		//$('#uploadfiles').trigger('click');
		$("input[id='uploadfiles']").click();
	});
	
});