/**
 * Created by anandsoni on 09/10/18.
 */
$( document ).ready( function() {

    var backgroundImage = $("#background_image");

    isMobile = function(){
        var isMobile = window.matchMedia("only screen and (max-width: 760px)");
        return isMobile.matches
    };

    if(isMobile()) {
        $("#searchBar").hide();
    }

    $(".loginBtn").click(function () {

        $("#register").removeClass("is-active");
        $("#forgetpasswordmodal").modal("hide");
        //$("#loginModal").modal("show");

    });

    $("#forgetpasswordBtn").click(function () {
        $("#register").removeClass("is-active");
        $("#loginModal").modal("hide");
        $("#forgetpasswordmodal").modal("show");
    });

    $(".delete, .modal-background").click(function () {
        $(".is-active").removeClass("is-active");
    });

    $(".registerBtn").click(function () {
        callRegisterModel()
    });

    backgroundImage.css("background-image", "url('./images/bg-cty-landing.jpg')");

    // Check for click events on the navbar burger icon
    $(".navbar-burger").click(function() {

        // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
        $(".navbar-burger").toggleClass("is-active");
        $(".navbar-menu").toggleClass("is-active");

    });

    //$('[data-toggle="datepicker"]').datepicker();


    // $('#drag-and-drop-zone').dmUploader({
    //     method: 'POST',
    //     extraData: {},
    //     maxFileSize: 0,
    //     maxFiles: 0,
    //     allowedTypes: '*',
    //     extFilter: null,
    //     dataType: null,
    //     fileName: 'file'
    // });

    // $('#drag-and-drop-zone').dmUploader({
    //     onInit: function(){},
    //     onFallbackMode: function(message) {},
    //     onNewFile: function(id, file){},
    //     onBeforeUpload: function(id){},
    //     onComplete: function(){},
    //     onUploadProgress: function(id, percent){},
    //     onUploadSuccess: function(id, data){},
    //     onUploadError: function(id, message){},
    //     onFileTypeError: function(file){},
    //     onFileSizeError: function(file){},
    //     onFileExtError: function(file){},
    //     onFilesMaxError: function(file){}
    // });

});

function showSearchBar() {

    var searchBar = $("#searchBar");

    if (!searchBar.is(':visible')) {
        searchBar.show();
        searchBar.focusin();
    } else {
        searchBar.hide();
    }
}

function callRegisterModel() {

    $("#loginModal").modal("hide");
    $("#register").addClass("is-active");
}

function switchToStudent() {
    removeActive();
    hideAllIndex();
    $("#tab_student").addClass("is-active");
    $("#tab_student_content").removeClass("is-hidden");
}

function switchToColleges() {
    removeActive();
    hideAllIndex();
    $("#tab_colleges").addClass("is-active");
    $("#tab_colleges_content").removeClass("is-hidden");
}

function switchToCompany() {
    removeActive();
    hideAllIndex();
    $("#tab_company").addClass("is-active");
    $("#tab_company_content").removeClass("is-hidden");
}

function switchToOrganisation() {
    removeActive();
    hideAllIndex();
    $("#tab_organisers").addClass("is-active");
    $("#tab_organisation_content").removeClass("is-hidden");
}

function removeActive() {
    $("li").each(function() {
        $(this).removeClass("is-active");
    });
}

function hideAllIndex(){
    $("#tab_student_content").addClass("is-hidden");
    $("#tab_colleges_content").addClass("is-hidden");
    $("#tab_company_content").addClass("is-hidden");
    $("#tab_organisation_content").addClass("is-hidden");
}

function switchToTimeline() {
    removeActive();
    hideAllProfile();
    $("#tab_timeline").addClass("is-active");
    $("#tab_timeline_content").removeClass("is-hidden");
}

function switchToAbout() {
    removeActive();
    hideAllProfile();
    $("#tab_about").addClass("is-active");
    $("#tab_about_content").removeClass("is-hidden");
}

function switchToCalender() {
    removeActive();
    hideAllProfile();
    $("#tab_calender").addClass("is-active");
    $("#tab_calender_content").removeClass("is-hidden");
}

function switchToEducational() {
    removeActive();
    hideAllProfile();
    $("#tab_education").addClass("is-active");
    $("#tab_education_content").removeClass("is-hidden");
}

function switchToFriendsList() {
    removeActive();
    hideAllProfile();
    $("#tab_friends").addClass("is-active");
    $("#tab_friends_content").removeClass("is-hidden");
}

function switchToPhotos() {
    removeActive();
    hideAllProfile();
    $("#tab_photos").addClass("is-active");
    $("#tab_photos_content").removeClass("is-hidden");
}

function removeActive() {
    $("li").each(function() {
        $(this).removeClass("is-active");
    });
}

function registerUser() {

    $("#form-register").submit();

}
function forgetPasswordUser() {

    $("#form-forgetPassword").submit();

}

function hideAllProfile(){
    $("#tab_timeline_content").addClass("is-hidden");
    $("#tab_about_content").addClass("is-hidden");
    $("#tab_calender_content").addClass("is-hidden");
    $("#tab_education_content").addClass("is-hidden");
    $("#tab_friends_content").addClass("is-hidden");
    $("#tab_photos_content").addClass("is-hidden");
}
